﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(AudioSource))]
public class GameMusicManager : MonoBehaviour
{
    public AudioClip yetiMusic;
    public AudioClip dragonMusic;
    public GameManager gameManager;

    private AudioSource _yetiMusicSource;
    private AudioSource _dragonMusicSource;
    private bool _yetiMusicPlaying;

    void Awake()
    {
        var audioSources = GetComponents<AudioSource>();
        if (audioSources.Length != 2)
            throw new Exception("2 audio sources needed");

        _yetiMusicSource = audioSources[0];
        _dragonMusicSource = audioSources[1];
        _yetiMusicSource.clip = yetiMusic;
        _dragonMusicSource.clip = dragonMusic;
        _yetiMusicSource.loop = true;
        _dragonMusicSource.loop = true;
        _yetiMusicSource.Play();
        _yetiMusicPlaying = true;
    }

    void Update()
    {
        var score = gameManager.Score;
        if (score < 50 && _yetiMusicPlaying) // dragon music
        {
            _yetiMusicSource.Pause();
            var time = _yetiMusicSource.time;
            _dragonMusicSource.time = time;
            _dragonMusicSource.Play();
            _yetiMusicPlaying = false;
        }
        if(score >= 50 && !_yetiMusicPlaying)// yeti music
        {
            _dragonMusicSource.Pause();
            var time = _dragonMusicSource.time;
            _yetiMusicSource.time = time;
            _yetiMusicSource.Play();
            _yetiMusicPlaying = true;
        }
    }

}

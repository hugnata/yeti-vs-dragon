﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Move))]
[RequireComponent(typeof(Rigidbody2D))]
public class Dash : MonoBehaviour
{
    public float force;
    public float cooldown;
    public string dashAxis;

    private Move _move;
    private Rigidbody2D _rigidBody2D;
    private float _nextTimeDash;
    
    private void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
        _move = GetComponent<Move>();
        _nextTimeDash = Time.time;
    }
    
    private void Update()
    {
        if (Input.GetButton(dashAxis) && (Time.time > _nextTimeDash))
        {
            _nextTimeDash = Time.time + cooldown;
            _rigidBody2D.AddForce(_move.Direction * force, ForceMode2D.Impulse);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisionManager : MonoBehaviour
{

    public AudioClip collision_Sound;
    public GameObject soundManager;
    private Rigidbody2D _rigidbody;
    public string Xaxe;
    public string Yaxe;
    public Vector2 Direction => new Vector2(_left, _up);

    private float _up;
    private float _left;


    public void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        GameObject collider = collision.gameObject;


        if (collider.tag.Equals("Player"))
        {
            _up = Input.GetAxis(Yaxe);
            _left = Input.GetAxis(Xaxe);
            Vector2 v = collider.transform.position - transform.position;
            Debug.Log(v + "  " + Direction);
            if (Vector2.Dot(v, Direction) > 0)
            {
               
                    this.GetComponent<Move>().Stun();
                    soundManager.GetComponent<SoundManager>().requestSound(collision_Sound);
               
            }
        }
    }

   
}

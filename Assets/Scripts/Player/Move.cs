﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Move : MonoBehaviour
{
    public float speed;
    public GameObject map;
    public Color yeti_color;
    public float radius;
    public string Xaxe;
    public string Yaxe;
    public Vector2 Direction => new Vector2(_left, _up);
    
    private float _up;
    private float _left;

    private Texture2D _texture;
    private bool _stunned = false;
    private Rigidbody2D _rigidbody;
    // Update is called once per frame

    public void Start()
    {
        _texture = map.GetComponent<SpriteRenderer>().sprite.texture;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        _up = Input.GetAxis(Yaxe);
        _left = Input.GetAxis(Xaxe);

        var transform1 = transform;
        var position = transform1.position;
        position.x += _left * speed * Time.deltaTime;
        position.y += _up * speed * Time.deltaTime;
        
        if (!_stunned)
        {
            transform1.SetPositionAndRotation(position, transform1.rotation);
        }
        
        for (int y = (int)(position.y*100 - radius); y < (int)(position.y*100 + radius); y++)
        {
            for (int x = (int)(position.x * 100 - radius); x < (int)(position.x * 100) + radius; x++)
            {
                var x_loc = x - position.x * 100;
                var y_loc = y - position.y * 100;
                if ((x_loc * x_loc + y_loc * y_loc) <= (radius * radius))
                {
                    _texture.SetPixel(x, y, yeti_color);
                }
            }
        }
        
        _texture.Apply();
    }

    public void Stun()
    {
        this._stunned = true;
        //Debug.Log(name + "Stunned !");
        StartCoroutine("StopStun");
    }

    private IEnumerator StopStun()
    {
        yield return new WaitForSeconds(2);
        //Debug.Log(name+"Not stunned anymore !");
        this._stunned = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {
        Texture2D texture = GetComponent<SpriteRenderer>().sprite.texture;
        for (int y =0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width/2; x++)
            {
                texture.SetPixel(x, y, Color.clear);
            }
            for (int x = texture.width/2; x < texture.width; x++)
            {
                texture.SetPixel(x, y, Color.white);
            }
        }

        texture.Apply();

    }

    // Update is called once per frame
    void Update()
    {


    }
}

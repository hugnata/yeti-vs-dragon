﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicDestroyHandler : MonoBehaviour
{
    public string previousMusic;

    void Awake()
    {
        var prevMusic = GameObject.Find(previousMusic);
        if (prevMusic != null)
            Destroy(prevMusic);
        DontDestroyOnLoad(gameObject);
    }
}

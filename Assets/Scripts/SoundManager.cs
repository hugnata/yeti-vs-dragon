﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource _audiosource;
    // Start is called before the first frame update
    void Start()
    {
        _audiosource = this.GetComponent<AudioSource>();

    }

    public void requestSound(AudioClip clip)
    {
        _audiosource.PlayOneShot(clip,1f);
    }
   
}

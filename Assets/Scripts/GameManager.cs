﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject masque;

    public float Score => score;

    private float score;
    private Texture2D _texture;

    private void Awake()
    {
        _texture = masque.GetComponent<SpriteRenderer>().sprite.texture;
    }

    private void Update()
    {
        score = ComputeScore();
    }

    public float ComputeScore()
    {
        float res = 0;

        for (int i = 0 ; i < _texture.width; i+= 10)
        {
            for (int j = 0; j < _texture.height; j += 10)
            {
                Color color = _texture.GetPixel(i, j);
                res += color.a;
            }
        }
        return (res*10000)/(_texture.width * _texture.height);
    }
}

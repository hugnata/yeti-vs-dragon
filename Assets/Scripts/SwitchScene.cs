﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
    public string scene;

    public void LoadScene()
    {
        Debug.Log("Switching to scene : " + scene);
        SceneManager.LoadScene(scene);
    }
}

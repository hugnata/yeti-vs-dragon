﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk : MonoBehaviour
{

    public string Xaxe;
    public string Yaxe;
    private float _up;
    private float _left;

    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _up = Input.GetAxis(Yaxe);
        _left = Input.GetAxis(Xaxe);

        _animator.SetFloat("x", _left);
    }
}
